// pages/info/info.js
const db = wx.cloud.database()

import "../../utils/throttle";
Page({
  /**
   * 页面的初始数据
   */
  data: {
    openid: "", //用户的openid
    photo: "../../images/role.png", //默认的用户头像
    name: "",
    address: "",
    tel: '',
    isperson: '', //用户是否存在

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    // 调用openid云函数
    wx.cloud.callFunction({
      name: 'openid',
    }).then(res => {
      // console.log(res.result.openid);
      this.setData({
        openid: res.result.openid
      })
      wx.setStorageSync('openid', res.result.openid)
      //获得用户信息
      this.getuserinfo()
    })

  },
  // 添加头像
  tapAvatar() {
    let that = this;

    wx.chooseMedia({
      count: 1,
      mediaType: ['image', 'video'],
      sourceType: ['album', 'camera'],
      maxDuration: 30,
      camera: 'back',
      success(res) {
        that.setData({
          photo: res.tempFiles[0].tempFilePath
        })
      }
    })

  },
  // 调用防抖函数（触发事件后在1秒内函数只能执行一次）


  //1、获得用户输入的姓名
  name(event) {
    const {
      value
    } = event.detail;

    const filteredValue = value.replace(/\s/g, ''); // 过滤掉空格
    console.log(filteredValue);
    this.setData({
      name: filteredValue

    });


  },
  //2、获得用户输入的地址
  address(e) {
    console.log("用户输入的地址", e.detail.value)
    this.setData({
      address: e.detail.value
    })
  },


  // 获取手机号码
  getPhoneNumber(e) {
    console.log("点击获取电话号码", e.detail.code)
    wx.cloud.callFunction({
      name: 'getPhoneNumber',
      data: {
        code: e.detail.code
      },
      success: (res) => {
        console.log("获得电话号码返回值", res.result.phoneInfo.purePhoneNumber)
        this.setData({
          tel: res.result.phoneInfo.purePhoneNumber
        })
      }
    })
  },
  getuserinfo() {
    let openid = wx.getStorageSync('openid')
    db.collection('userinfo').where({
      openid: openid
    }).get().then(res => {
      this.setData({
        isperson: res.data.length,
        name: res.data[0].name,
        address: res.data[0].address,
        tel: res.data[0].tel,
        photo: res.data[0].photo
      })
    })

  },
  saveinfo() {
    // 校验当前头像、姓名、电话、地址为空的情况
    if (this.data.photo == '../../images/role.png') {
      wx.showToast({
        title: '请选择上传头像信息',
      })
      return
    }

    if (this.data.name.length == 0) {
      wx.showToast({
        title: '请填写姓名',
        icon: 'none'
      })
      return
    }
    if (this.data.tel.length == 0) {
      wx.showToast({
        title: '请填写电话',
        icon: 'none'
      })
      return
    }
    if (this.data.address.length == 0) {
      wx.showToast({
        title: '请填写地址',
        icon: 'none'
      })
      return
    }
    // 1、修改用户信息
    if (this.data.isperson) {
      // 1、修改用户信息
      let openid = wx.getStorageSync('openid')
      db.collection("userinfo").where({
        openid: openid
      }).update({
        data: {
          openid: openid,
          name: this.data.name,
          photo: this.data.photo,
          tel: this.data.tel,
          address: this.data.address
        }
      }).then(res => {
        wx.showToast({
          title: '修改成功',
        })
      })
    } else {
      // 2、保存用户信息
      let openid = wx.getStorageSync('openid')
      db.collection("userinfo").add({
        data: {
          openid: openid,
          photo: this.data.photo,
          address: this.data.address,
          tel: this.data.tel,
          name: this.data.name,
        }
      }).then(res => {
        console.log(res);
        //保存成功之后，再次获得用户信息，更新isperson的值
        wx.showToast({
          title: '保存成功',
        })
      })
    }

  },



  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {},

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    let openid = wx.cloud.callFunction({
      name: 'openid'
    }).then(res => {
      console.log("云函数获得的数据", res.result.openid)
      this.setData({
        openid: res.result.openid
      })
      wx.setStorageSync('openid', res.result.openid)
      //获得用户信息
      this.getuserinfo()
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {},

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {},

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {},

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {},

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {},
});