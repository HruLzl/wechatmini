// 定义一个全局的计时器变量
let timer = null;

// 防抖函数
function debounce(func, delay) {
  // 清除之前的计时器
  clearTimeout(timer);

  // 创建新的计时器
  timer = setTimeout(function() {
    func.apply(this, arguments);
  }, delay);
}

